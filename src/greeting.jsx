export const Greeting = ({ title, name = "User" }) => {
  // export const Greeting = (props) => {
  return (
    <div>
      <h1>{`${title}, says ${name}`}</h1>
    </div>
  );
};
/* Should use PascalCase or it doesn't work as tag, it's a way to difference the normal functions and the JSX components
  //<Greeting/> => Good -- <greeting/> show nothing
  ----
  .jsx is not required, but it tells that here you're working with componets
  */

export const UserCard = (props) => {
  return (
    <>
      <h1>{props.name}</h1>
      <p>💵 {props.amount}</p>
      <p>{props.married ? "married" : "single"}</p>
      <h5>
        Address:  {`${props.address.street}, ${props.address.city}, ${props.address.state} `}
      </h5>
    </>
  );
};
