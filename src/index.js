import { React } from "react";
import ReactDOM from "react-dom/client";
import { Greeting, UserCard } from "./greeting.jsx";
import { Button } from "./button.jsx";
import { TaskCard } from "./task.jsx";
import { SayHello } from "./sayHello.jsx";
import { Posts } from "./posts.jsx";

const rootElement = document.getElementById("root");

const root = ReactDOM.createRoot(rootElement);

const handleChange = (event) => {
  console.log(event.target.value);
};

root.render(
  <>
    <TaskCard ready={false} />
    <TaskCard ready={true} />
    <SayHello />
    <hr></hr>
    <h4>Event Handlers</h4>
    <Button text="Hello" />
    <input id="hello" onChange={handleChange}></input>
    <form
      onSubmit={(e) => {
        e.preventDefault();
        console.log("Send!");
        alert("Send!");
      }}
    >
      <h4>Form</h4>
      <button>Send</button>
    </form>
    <hr></hr>
    <h4>Fetch API and third party-modules(react icons)</h4>
    <Posts />
  </>
);
