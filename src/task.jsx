import "./task-component.css";

export function TaskCard({ ready }) {
  const cardStyles = {
    background: "#303330",
    color: "#fff",
    padding: "20px",
    margin: "5px",
  };
  return (
    <div className="card" style={cardStyles}>
      <h3>My first task!</h3>
      <span className={ready ? "sp-green" : 'sp-red'}>
        {ready ? "Task done" : "Pending task"}
      </span>
    </div>
  );
}
