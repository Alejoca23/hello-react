async function fetch_data() {
    try {
      /*
                onClick={() => {
          fetch("https://jsonplaceholder.typicode.com/posts")
            .then((response) => response.json())
            .then((data) => console.log(data))
            .catch((error) => console.error(error));
        }}
          */
      const result = (
        await fetch("https://jsonplaceholder.typicode.com/posts")
      ).json();
      // let data = result.then(data => data[5]);
      let data = result.data;
      // console.log(data[0,8]);
      console.log(data);
      return data;
    } catch (e) {
      console.error(e);
    }
  }