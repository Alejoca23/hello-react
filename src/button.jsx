import PropTypes from "prop-types";

export function Button({ text, name }) {
  return (
    <div>
      <button
        onClick={() => {
          console.log("Hello World!");
        }}
      >{`${text}- ${name}`}</button>
    </div>
  );
}

Button.propTypes = {
  text: PropTypes.string,
};

Button.defaultProps = {
  name: "Random user",
};
